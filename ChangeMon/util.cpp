﻿#include "stdafx.h"

#include <vector>

#include "util.hpp"


std::string
to_utf8(const std::wstring& str)
{
	auto utf8 = std::vector<char>(str.size() * 4);
	if(const auto bytesWritten = ::WideCharToMultiByte(CP_UTF8, 0, str.c_str(), static_cast<int>(str.size()), utf8.data(), static_cast<int>(utf8.size()), nullptr, nullptr);
	   bytesWritten > 0)
	{
		// We passed an input size to WideCharToMultiByte(), so the output buffer is not null-terminated
		// and bytesWritten does not include the null character.
		return std::string{ utf8.data(), utf8.data() + bytesWritten };
	}
	return u8"�";
}
