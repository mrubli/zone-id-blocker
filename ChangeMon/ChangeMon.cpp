﻿#include "stdafx.h"

#include <array>
#include <iostream>
#include <string>

#define SPDLOG_WCHAR_TO_UTF8_SUPPORT
#include <spdlog/spdlog.h>
#include <spdlog/sinks/stdout_color_sinks.h>
#include <fmt/ostream.h>

#include "util.hpp"


namespace
{

	auto ConsoleLoggerPtr = std::make_shared<spdlog::logger>("cmon");

	auto& con = *ConsoleLoggerPtr;


	bool
	SetupLogging(bool verbose) try
	{
		// See: https://github.com/gabime/spdlog/issues/762
		::SetConsoleOutputCP(CP_UTF8);

		// Create a colored console sink that goes to stderr
		auto console_sink = std::make_shared<spdlog::sinks::stderr_color_sink_mt>();
		//console_sink->set_color(spdlog::level::info,  console_sink->reset); // No color for info level
		//console_sink->set_color(spdlog::level::debug, console_sink->white); // Gray for debug level
		if(!verbose)
		{
			// In non-verbose mode only show the message. In verbose mode we want the timestamps, so use the default
			// there.
			console_sink->set_pattern("%^%v%$");                            // Only the message but in color
		}

		// Use the console for the console logger
		con.sinks().push_back(console_sink);

		// Set the log level at the logger level, not the sink level
		con.set_level(verbose ? spdlog::level::debug : spdlog::level::warn);

		// Make our console logger the global logger, so that libraries we load use it by default
		spdlog::set_default_logger(ConsoleLoggerPtr);

		return true;
	}
	catch(const spdlog::spdlog_ex& ex)
	{
		std::cout << "ERROR: Logging setup failed: " << ex.what() << std::endl;
		return false;
	}


	enum FileNotifyInfoAction : DWORD
	{
		Added			= 0x00000001,
		Removed			= 0x00000002,
		Modified		= 0x00000003,
		RenamedOldName	= 0x00000004,
		RenamedNewName	= 0x00000005,
	};

	std::wostream&
	operator<<(std::wostream& os, FileNotifyInfoAction action)
	{
		switch(action)
		{
			case Added:				os << L"ADDED";				break;
			case Removed:			os << L"REMOVED";			break;
			case Modified:			os << L"MODIFIED";			break;
			case RenamedOldName:	os << L"RENAMED_OLD_NAME";	break;
			case RenamedNewName:	os << L"RENAMED_NEW_NAME";	break;
			default:				os << L"<Unknown>";			break;
		}
		return os;
	}


	bool
	DumpStream(const std::wstring& fileName, const std::wstring& streamName)
	{
		const auto fullName = fileName + streamName;
		const auto hFile = ::CreateFile(
			fullName.c_str(),
			GENERIC_READ,
			FILE_SHARE_READ,
			nullptr,
			OPEN_EXISTING,
			FILE_ATTRIBUTE_NORMAL,
			nullptr
		);
		if(hFile == INVALID_HANDLE_VALUE)
		{
			const auto error = ::GetLastError();
			con.error(L"Unable to open file stream '{}': {}", fullName, error);
			return false;
		}

		auto buffer = std::array<char, 2 * 1024>{};
		auto bytesRead = DWORD{};
		if(const auto success = ::ReadFile(hFile, buffer.data(), static_cast<DWORD>(buffer.size() - 1), &bytesRead, nullptr);
		   !success)
		{
			const auto error = ::GetLastError();
			con.error(L"Unable to read from '{}': {}", fullName, error);
			::CloseHandle(hFile);
			return false;
		}
		buffer[bytesRead] = '\0';
		::CloseHandle(hFile);

		// fmtlib refuses to mix character types, so we convert the name to UTF-8
		con.info("Dumping contents of '{}':\r\n========================================\r\n{}\r\n========================================",
				 to_utf8(fullName), buffer.data());

		return true;
	}


	bool
	EnumerateStreams(const std::wstring& fileName)
	{
		con.info(L"Streams of '{}':", fileName);

		const auto hFile = ::CreateFile(
			fileName.c_str(),
			GENERIC_READ,
			FILE_SHARE_WRITE | FILE_SHARE_READ | FILE_SHARE_DELETE,
			nullptr,
			OPEN_EXISTING,
			FILE_ATTRIBUTE_NORMAL,
			nullptr
		);
		if(hFile == INVALID_HANDLE_VALUE)
		{
			const auto error = ::GetLastError();
			con.error(L"Unable to open file '{}' for enumerating streams: {}", fileName, error);
			return false;
		}

		auto evilDetected = false;

		uint8_t buffer[32 * 1024];
		static_assert(sizeof(buffer) > sizeof(WIN32_STREAM_ID));	// The BackupRead doc says so
		VOID *pContext = nullptr;
		for(;;)
		{
			// Read the stream header (plus a whole bunch of other data because we don't know how
			// large that header really is. We could read just sizeof(WIN32_STREAM_ID) first and
			// then read the file name but this is easier.
			auto dwNumberOfBytesRead = DWORD{};
			{
				const auto success = ::BackupRead(
					hFile,
					buffer,
					sizeof(buffer),
					&dwNumberOfBytesRead,
					FALSE,
					FALSE,
					&pContext
				);
				if(!success)
				{
					const auto error = ::GetLastError();
					con.error("BackupRead() failed: {}", error);
					break;
				}
				// Note: dwNumberOfBytesRead may be larger than the size of the WIN32_STREAM_ID struct
				//       plus the stream name plus the stream data. Not sure what that's about.
			}
			if(dwNumberOfBytesRead == 0)
				break;	// All the data associated with the file handle has been read

			const auto& si = *reinterpret_cast<const WIN32_STREAM_ID *>(buffer);

			{
				const auto name = std::wstring{ si.cStreamName, si.dwStreamNameSize / sizeof(si.cStreamName[0]) };

				con.info(L"  {{ Name: '{}', Id: {}, Size: {} }}", name, si.dwStreamId, si.Size.QuadPart);

				if(name == L":Zone.Identifier:$DATA")
				{
					evilDetected = true;
					DumpStream(fileName, name);
					// Keep looking for other streams, just for displaying purposes
				}
			}

			// Seek to the next stream
			auto bytesToSeek = LARGE_INTEGER{};
			bytesToSeek.QuadPart = si.Size.QuadPart - (dwNumberOfBytesRead - offsetof(WIN32_STREAM_ID, cStreamName) - si.dwStreamNameSize);
			if(bytesToSeek.QuadPart > 0)
			{
				auto dwLowByteSeeked = DWORD{}, dwHighByteSeeked = DWORD{};
				const auto success = ::BackupSeek(
					hFile,
					bytesToSeek.LowPart, bytesToSeek.HighPart,
					&dwLowByteSeeked, &dwHighByteSeeked,
					&pContext
				);
				if(!success)
				{
					const auto error = ::GetLastError();
					con.error("BackupSeek() failed: {}", error);
					break;
				}
			}

		}

		// Abort the backup
		{
			const auto success = ::BackupRead(
				hFile,
				nullptr,
				0,
				nullptr,
				TRUE,
				FALSE,
				&pContext
			);
			if(!success)
			{
				const auto error = ::GetLastError();
				con.error("BackupRead(bAbort = TRUE) failed: {}", error);
			}
		}

		::CloseHandle(hFile);

		return evilDetected;
	}


	void
	ParseFileNotifyInfo(const FILE_NOTIFY_INFORMATION *list, const std::wstring& dirName)
	{
		auto entry = list;
		while(entry != nullptr)
		{
			const auto fileName = std::wstring{ entry->FileName, entry->FileNameLength / sizeof(entry->FileName[0]) };
			const auto action = static_cast<FileNotifyInfoAction>(entry->Action);

			con.info(L"{{ FileName: '{}', Action: {}, NextEntryOffset: {} }}", fileName, action, entry->NextEntryOffset);

			// We only care about 'Added' and 'RenamedNewName'.
			// The 'Modified' event sounds promising but adding a new stream to an existing file,
			// e.g. in PowerShell, doesn't trigger a notification.
			if(action == Added || action == RenamedNewName)
			{
				// TODO The ":Zone.Identifier:$DATA" stream gets added after we receive the notification,
				// so if e.g. the browser finishes downloading a file and we get notified the stream isn't
				// there yet. For now let's just use a simple sleep.
				Sleep(1000);
				const auto evilDetected = EnumerateStreams(dirName + L"\\" + fileName);
				if(evilDetected)
				{
					const auto evilName = dirName + L"\\" + fileName + L":Zone.Identifier:$DATA";
					con.warn(L"Annihilating evil: {}", evilName);
					const auto success = ::DeleteFile(evilName.c_str());
					if(!success)
					{
						const auto error = ::GetLastError();
						con.error("DeleteFile() failed: {}", error);
					}
				}
			}

			if(entry->NextEntryOffset == 0)
				entry = nullptr;
			else
				entry = reinterpret_cast<const FILE_NOTIFY_INFORMATION *>(reinterpret_cast<const uint8_t *>(entry) + entry->NextEntryOffset);
		}
	}

}

int
wmain(int argc, wchar_t *argv[], wchar_t *envp[])
{
	SetupLogging(true);
	con.info("ChangeMon");

	if(argc < 2)
	{
		con.error("No arguments given");
		return 1;
	}

	const auto dirName = std::wstring{ argv[1] };

	con.info(L"Monitoring directory: {}", dirName);
	auto hDir = ::CreateFile(
		dirName.c_str(),
		FILE_LIST_DIRECTORY,
		FILE_SHARE_WRITE | FILE_SHARE_READ | FILE_SHARE_DELETE,
		nullptr,
		OPEN_EXISTING,
		FILE_FLAG_BACKUP_SEMANTICS,
		nullptr
	);
	if(hDir == INVALID_HANDLE_VALUE)
	{
		const auto error = ::GetLastError();
		con.error(L"Unable to create file handle for monitoring directory {}: {}", dirName, error);
		return 2;
	}

	uint8_t buffer[32 * 1024];
	for(;;)
	{
		auto dwBytesReturned = DWORD{};
		const auto success = ::ReadDirectoryChangesW(
			hDir, buffer, sizeof(buffer), TRUE,
			FILE_NOTIFY_CHANGE_FILE_NAME | FILE_NOTIFY_CHANGE_DIR_NAME | FILE_NOTIFY_CHANGE_ATTRIBUTES | FILE_NOTIFY_CHANGE_SIZE,
			&dwBytesReturned,
			nullptr, nullptr
		);
		if(!success)
		{
			const auto error = ::GetLastError();
			con.error("ReadDirectoryChangesW() failed: {}", error);
			return 2;
		}

		ParseFileNotifyInfo(reinterpret_cast<FILE_NOTIFY_INFORMATION *>(buffer), dirName);
	}

	::CloseHandle(hDir);

	return 0;
}
