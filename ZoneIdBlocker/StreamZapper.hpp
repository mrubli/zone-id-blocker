#pragma once

#include <atomic>
#include <deque>
#include <thread>
#include <vector>

#include "basic_result.hpp"


class StreamZapper
{
public:
	StreamZapper();
	~StreamZapper();

	struct Event
	{
		const std::wstring&		FilePath;
		const std::wstring&		StreamName;
		bool					Success{};
	};
	using EventHandler = std::function<void(const Event&)>;
	void			SetEventHandler(EventHandler handler);

	bool			QueueFile(const std::wstring& filePath);

private:
	EventHandler			eventHandler_;

	std::thread				thread_;
	std::atomic<bool>		exitThread_{};

	struct File
	{
		std::wstring			Path;
		int						RetriesLeft{};
	};
	std::deque<File>		queue_;
	std::mutex				mutex_;
	std::condition_variable	workToDo_;

private:
	void					Thread();
	bool					ProcessFile(const File& file);
	basic_result<std::vector<std::wstring>, DWORD>
							ScanForBadStreams(const std::wstring& filePath);
	bool					DeleteStreams(const std::wstring& filePath, const std::vector<std::wstring>& streamNames);

	static bool				IsBadStream(const std::wstring& streamName);
};