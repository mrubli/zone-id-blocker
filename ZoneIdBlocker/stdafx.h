// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#define NOMINMAX
#include <Windows.h>

#include <string>

#include <fmt/format.h>

#define SPDLOG_WCHAR_TO_UTF8_SUPPORT
#include <spdlog/spdlog.h>
#include <spdlog/sinks/msvc_sink.h>

#include "log.hpp"
#include "scope_guard.hpp"
#include "util.hpp"
#include "WinException.hpp"
