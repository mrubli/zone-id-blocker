#include "stdafx.h"

#include <memory>

#include <comdef.h>
#include <CommCtrl.h>

#include "config.hpp"
#include "ChangeMonitor.hpp"
#include "StreamZapper.hpp"

#include "resource.h"


namespace
{

	//
	// Constants and Globals
	//

	const auto AppName = L"ZoneIdBlocker";

	auto Instance = HINSTANCE{};
	
#ifdef USE_NOTIFY_GUID
	class __declspec(uuid("78E50A4B-57E1-440F-B763-A43BF5D19442")) TrayIcon;
#else
	constexpr auto TrayIconId = 420;
#endif

	constexpr auto WMAPP_TRAY_MESSAGE = WM_APP + 1;

	auto NotifyWindowHandle = HWND{};

	std::unique_ptr<ChangeMonitor> changeMonitor;

	std::unique_ptr<StreamZapper> streamZapper;


	//
	// Helper functions
	//

	__declspec(noreturn)
	void
	ExitWithError(const std::wstring& error)
	{
		con.error(L"Fatal error: {}", error);
		::MessageBox(NULL, error.c_str(), AppName, MB_ICONERROR);
		::CoUninitialize();
		::ExitProcess(1);
	}

	void
	ExitWithError(const std::wstring& error, HRESULT hr)
	{
		_com_error err{ hr };
		ExitWithError(error + L": " + err.ErrorMessage());
	}

	void
	ExitWithError(const std::wstring& error, const std::exception& e)
	{
		ExitWithError(error + L": " + ascii_to_wstring(e.what()));
	}


	bool
	ReadSettings()
	{
		auto result = ConfigResult::Success;
		auto error = std::wstring{};
		std::tie(result, error) = read_config_file();
		switch(result)
		{
			case ConfigResult::Success:
				break;
			case ConfigResult::FileNotFound:
				init_default_config();	// Load default settings
				write_config_file();	// Write the current settings to the settings file
				break;
			case ConfigResult::SyntaxError:
			{
				const auto response = ::MessageBox(NULL,
					(L"An error has occurred while reading the configuration file:\r\n\r\n" + error + L"\r\n\r\n"
						L"Do you want to write a new configuration file containing default settings?\r\n"
						L"Selecting No will continue using default settings."
						).c_str(),
					AppName,
					MB_YESNO + MB_ICONERROR
				);
				if(response == IDYES)
				{
					init_default_config();
					write_config_file();
				}
				// Continue with the default settings ...
				break;
			}
			case ConfigResult::ConfigError:
			{
				const auto response = ::MessageBox(NULL,
					(L"Errors have occurred while reading the configuration file:\r\n\r\n" + error + L"\r\n\r\n"
					 L"Do you want to write a new configuration file while preserving existing valid settings?\r\n"
					 L"Selecting No will continue using a mix of valid configuration file settings and default settings.\r\n"
					 L"Select Cancel to exit the application."
					).c_str(),
					AppName,
					MB_YESNOCANCEL + MB_ICONERROR
				);
				if(response == IDYES)
				{
					write_config_file();
				}
				else if(response == IDCANCEL)
				{
					return false;
				}
				// Continue with the semi-valid settings and hope for the best ...
				break;
			}
		}

		return true;
	}


	void
	CreateNotificationIcon()
	{
		con.debug("Creating notification icon.");

		NOTIFYICONDATA nid = { sizeof(nid) };
		nid.uFlags = NIF_ICON | NIF_MESSAGE | NIF_TIP | NIF_SHOWTIP;
#ifdef USE_NOTIFY_GUID
		nid.uFlags |= NIF_GUID;
		nid.guidItem = __uuidof(TrayIcon);
#else
		nid.uID = TrayIconId;
#endif
		nid.hWnd = NotifyWindowHandle;
		nid.uCallbackMessage = WMAPP_TRAY_MESSAGE;
		{
			// Use LoadIconMetric instead of LoadImage to get the right size
			// Note that this requires linking in comctl32.lib as well as a special /manifestdependency
			// linker option. Also see:
			// https://social.msdn.microsoft.com/Forums/vstudio/en-US/722193a4-7fba-4ed9-a41f-6629efdb2a78
			const auto hr = ::LoadIconMetric(Instance, IDI_APPLICATION, LIM_SMALL, &nid.hIcon);
			if(FAILED(hr))
			{
				con.error("Unable to load notificaton icon using system metric LIM_SMALL: {:#x}. Falling back to default size.", hr);
				nid.hIcon = (HICON)::LoadImage(Instance, IDI_APPLICATION, IMAGE_ICON, 0, 0, LR_DEFAULTSIZE);
			}
		}
		wcscpy_s(nid.szTip, AppName);
		{
			const auto success = (::Shell_NotifyIcon(NIM_ADD, &nid) == TRUE);
			if(!success)
				throw WinException(E_FAIL, L"Failed to create notification icon");
		}
		nid.uVersion = NOTIFYICON_VERSION_4;
		{
			const auto success = (::Shell_NotifyIcon(NIM_SETVERSION, &nid) == TRUE);
			if(!success)
				throw WinException(E_FAIL, L"Failed to set notification API version");
		}
	}


	void
	DestroyNotificationIcon()
	{
		con.debug("Destroying notification icon.");

		NOTIFYICONDATA nid = { sizeof(nid) };
		nid.uFlags = NIF_GUID;
#ifdef USE_NOTIFY_GUID
		nid.uFlags |= NIF_GUID;
		nid.guidItem = __uuidof(TrayIcon);
#else
		nid.uID = TrayIconId;
#endif
		const auto success = (::Shell_NotifyIcon(NIM_DELETE, &nid) == TRUE);
		if(!success)
		{
			con.error("Unable to destroy notification icon.");
		}
	}


	void
	DisplayNotificationMenu()
	{
		const auto hMenu = ::LoadMenu(Instance, MAKEINTRESOURCE(IDR_TRAY_MENU));
		if(hMenu == NULL)
		{
			con.error("Unable to load context menu.");
			return;
		}

		const auto hSubMenu = ::GetSubMenu(hMenu, 0);
		if(hSubMenu == NULL)
		{
			con.error("Unable to load context submenu.");
			return;
		}

		POINT cursorPos = { 0, 0 };
		::GetCursorPos(&cursorPos);

		const auto success = ::TrackPopupMenu(hSubMenu, TPM_RIGHTBUTTON, cursorPos.x, cursorPos.y, 0, NotifyWindowHandle, NULL);
		if(!success)
		{
			con.error("Unable to display context menu.");
			return;
		}
	}


	enum class Severity { Info, Warning, Error };

	bool
	DisplayBalloonMessage(const std::wstring& message, Severity severity)
	{
		con.debug(L"Displaying balloon notification: \"{}\"", message);

		NOTIFYICONDATA nid = { sizeof(nid) };
		nid.uFlags = NIF_INFO;
		nid.hWnd = NotifyWindowHandle;
#ifdef USE_NOTIFY_GUID
		nid.uFlags |= NIF_GUID;
		nid.guidItem = __uuidof(TrayIcon);
#else
		nid.uID = TrayIconId;
#endif
		nid.dwInfoFlags = [severity] {
			switch(severity)
			{
				case Severity::Info:	return NIIF_INFO;
				case Severity::Warning:	return NIIF_WARNING;
				case Severity::Error:	return NIIF_ERROR;
				default:
					assert(false);
					return NIIF_INFO;
			}
		}();
		wcscpy_s(nid.szInfoTitle, AppName);
		wcscpy_s(nid.szInfo, message.c_str());
		const auto success = (::Shell_NotifyIcon(NIM_MODIFY, &nid) == TRUE);
		if(!success)
		{
			con.error("Unable to display balloon notification.");
			if(severity == Severity::Error)
			{
				// Display an error message instead if this is an error
				::MessageBox(NULL, message.c_str(), AppName, MB_ICONERROR);
			}
		}
		return success;
	}


	void
	CreateChangeMonitor()
	{
		try
		{
			changeMonitor = std::make_unique<ChangeMonitor>();
		}
		catch(const std::exception& e)
		{
			con.error("Failed to create change monitor: {}", e.what());
			throw;
		}

		try
		{
			streamZapper = std::make_unique<StreamZapper>();
		}
		catch(const std::exception& e)
		{
			con.error("Failed to create stream zapper: {}", e.what());
			throw;
		}

		changeMonitor->SetEventHandler([] (const ChangeMonitor::Event& event) {
			streamZapper->QueueFile(event.FileName);
		});

		streamZapper->SetEventHandler([] (const StreamZapper::Event& event) {
			const auto message = fmt::format(L"{} unwanted stream {}:{}",
											 event.Success ? L"Deleted" : L"Failed to delete",
											 event.FilePath, event.StreamName);
			DisplayBalloonMessage(message, event.Success ? Severity::Info : Severity::Warning);
		});

		// Start monitoring all directories given in the config file
		for(const auto& dir : Settings.Directories)
		{
			const auto success = changeMonitor->AddDirectory(dir);
			if(!success)
			{
				const auto error = fmt::format(L"Unable to monitor directory '{}'", dir);
				con.error(L"{}", error);
				::MessageBox(NULL, error.c_str(), AppName, MB_ICONERROR);
			}
		}
	}


	void
	DestroyChangeMonitor()
	{
		changeMonitor.reset();
		streamZapper.reset();
	}


	LRESULT
	CALLBACK
	NotifyWindowProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
	{
#if 0
		con.debug("[NotifyWindowProc] {:#x}", uMsg);
#endif

		switch(uMsg)
		{
			case WM_CREATE:
			{
				con.debug("WM_CREATE");
				//if(!CreateChangeMonitor())
				//	return -1;
				break;
			}
			case WM_DESTROY:
			{
				con.debug("WM_DESTROY");
				//DestroyChangeMonitor();
				break;
			}
			case WM_COMMAND:
				switch(LOWORD(wParam))
				{
					case IDM_TRAY_EXIT:
						::PostQuitMessage(0);
						break;
				}
				break;

			case WMAPP_TRAY_MESSAGE:
				switch(LOWORD(lParam))	// NOTIFYICON_VERSION_4 style
				{
					case WM_CONTEXTMENU:
						DisplayNotificationMenu();
						break;
				}
				break;
		}

		return ::DefWindowProc(hWnd, uMsg, wParam, lParam);
	}


	void
	CreateNotifyWindow()
	{
		WNDCLASSEX notifyWindowClass;
		::ZeroMemory(&notifyWindowClass, sizeof(notifyWindowClass));
		notifyWindowClass.cbSize = sizeof(notifyWindowClass);
		notifyWindowClass.style = 0;
		notifyWindowClass.lpfnWndProc = NotifyWindowProc;
		notifyWindowClass.hInstance = Instance;
		notifyWindowClass.lpszClassName = L"ZoneIdBlockerNotifyWindowClass";
		const auto ret = ::RegisterClassEx(&notifyWindowClass);
		if(ret == 0)
			throw WinException(::GetLastError(), L"Failed to register notification window class");

		// Create a message-only window. Note that this window does not receive broadcast messages.
		NotifyWindowHandle = ::CreateWindow(L"ZoneIdBlockerNotifyWindowClass", NULL, 0, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, HWND_MESSAGE, NULL, Instance, NULL);
		if(NotifyWindowHandle == NULL)
			throw WinException(::GetLastError(), L"Failed to create notification window");
	}

}


int
WINAPI
WinMain(
	HINSTANCE hInstance,
	HINSTANCE hPrevInstance,
	LPSTR lpCmdLine,
	int nCmdShow
)
{
	Instance = hInstance;

	SetupLogging();

	if(const auto success = ReadSettings(); !success)
	{
		ExitWithError(L"Failed to initialize settings.");
	}

	// Initialize COM
	{
		auto hr = ::CoInitializeEx(NULL, COINIT_MULTITHREADED);
		if(FAILED(hr))
		{
			ExitWithError(L"Failed to initialize COM", hr);
		}
	}
	auto uninitializeCom = make_scope_guard([] { ::CoUninitialize(); });

	// Initialize the tray icon and all message handling
	try
	{
		CreateNotifyWindow();
		CreateNotificationIcon();
		CreateChangeMonitor();
	}
	catch(const std::exception& e)
	{
		ExitWithError(L"Failed to initialize", e);
	}

	// Main message loop
	{
		BOOL success;
		MSG msg;
		while ((success = ::GetMessage(&msg, NotifyWindowHandle, 0, 0)) != 0)
		{
			if (success == -1)
			{
				ExitWithError(L"GetMessage() failed");
			}

			::TranslateMessage(&msg);
			::DispatchMessage(&msg);
		}
	}

	// Clean up
	DestroyChangeMonitor();
	DestroyNotificationIcon();

	return 0;
}
