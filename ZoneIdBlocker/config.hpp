﻿#pragma once

#include <string>
#include <tuple>
#include <vector>


struct ProgramSettings
{
	std::vector<std::wstring>	Directories;
	std::vector<std::wstring>	UnwantedStreams{ L":Zone.Identifier:$DATA" };
};

extern ProgramSettings Settings;


void
init_default_config();

enum class ConfigResult { Success, FileNotFound, SyntaxError, ConfigError };

// Returns a { result, errorString } tuple where errorString is a human-readable error message and
// result is one of the following:
// - Success if no error occurred.
// - FileNotFound if the config file cannot be opened. (The current settings are not modified.)
// - SyntaxError if the config file cannot be parsed. (The current settings are not modified.)
// - ConfigError if at least one setting was erroneous. (Default settings were loaded and overridden
//   with whatever settings could be loaded from the config file.)
std::tuple<ConfigResult, std::wstring>
read_config_file();

bool
write_config_file();
