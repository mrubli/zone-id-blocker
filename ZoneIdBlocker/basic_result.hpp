#pragma once

#include <iostream>
#include <optional>
#include <type_traits>
#include <variant>
#include <cassert>


template<typename T, typename E>
class basic_result final
{
public:
	using value_type = T;
	using error_type = E;

	static_assert(!std::is_same<value_type, error_type>::value, "Value type T must not be the same as error type E");

	basic_result() = delete;

	// Constructs a result with the given value
	basic_result(const value_type& value) : content_{ value } {}
	basic_result(value_type&& value) : content_{ std::move(value) } {}

	// Constructs a result by converting the given value to a value_type
	template<typename U, typename = typename std::enable_if<!std::is_same<typename std::decay<U>::type, basic_result>::value>::type>
	basic_result(U&& value) : content_{ value_type{ std::forward<U>(value) } } {}

	// Constructs a result with the given error value
	basic_result(error_type error) : content_{ error } {}

	// Copy/move constructor
	basic_result(const basic_result&) = default;
	basic_result(basic_result&&) = default;

	// Copy/move assignment operators
	basic_result& operator=(const basic_result& rhs) = default;
	basic_result& operator=(basic_result&& rhs) = default;

	// Dereference operators for accessing the value. Throws if the result contains an error status.
	const value_type& operator*() const { return value(); }
	value_type& operator*() { return value(); }

	const value_type* operator->() const { return &value(); }
	value_type* operator ->() { return &value(); }

	// Checks if the result contains an error status (false) or a success status (true).
	bool has_value() const { return content_.index() == 0; }

	explicit operator bool() const { return has_value(); }

	// Returns the error. Throws if the result contains a value.
	const error_type& error() const { assert(!has_value()); return std::get<error_type>(content_); }

	// Returns the value. Throws if the result contains an error.
	const value_type& value() const { assert(has_value()); return std::get<value_type>(content_); }
	value_type& value() { assert(has_value()); return std::get<value_type>(content_); }

	// Returns the value or the provided default value if the result contains an error status
	template<typename U>
	value_type value_or(U&& defaultValue) const
	{
		return has_value() ? **this : static_cast<value_type>(std::forward<U>(defaultValue));
	}

	template<typename U>
	value_type value_or(U&& defaultValue)
	{
		return has_value() ? std::move(**this) : static_cast<value_type>(std::forward<U>(defaultValue));
	}

	// If the result contains a value, invokes the given callable with the value as an argument.
	// The callable must itself return a basic_result<U, E> where E must be the same as this
	// result's E but U may be a different type from this result's T.
	// The return value is of type basic_result<U, E>.
	// If the result contains an error, basic_result<U, E>{ error() } is returned.
	template<typename F>
	auto and_then(F&& f) const -> basic_result<typename std::result_of<F(T)>::type::value_type, E>
	{
		if(has_value())
			return f(value());
		return error();
	}

	// Converts the result into an optional<T> which contains the value or is empty if the
	// result contains an error.
	std::optional<T> ok() const
	{
		if(has_value())
			return value();
		return {};
	}

	std::optional<T> ok()
	{
		if(has_value())
			return std::move(value());
		return {};
	}

private:
	std::variant<value_type, error_type>    content_;

public:
	friend std::ostream& operator<<(std::ostream& os, const basic_result& r)
	{
		if(r)
		{
			os << r.value();
		}
		else
		{
			os << "<ERROR: " << r.error() << '>';
		}
		return os;
	}
};


template<typename U, typename T, typename E>
auto
result_cast(const basic_result<T, E>& result) -> basic_result<U, E>
{
	if(result)
		return static_cast<U>(result.value());
	return result.error();
}

template<typename U, typename F, typename T, typename E>
auto
result_cast(const basic_result<T, E>& result) -> basic_result<U, F>
{
	if(result)
		return static_cast<U>(result.value());
	return static_cast<F>(result.error());
}
