#include "stdafx.h"

#include <cassert>

#include "ChangeMonitor.hpp"


namespace
{

	enum FileNotifyInfoAction : DWORD
	{
		Added			= 0x00000001,
		Removed			= 0x00000002,
		Modified		= 0x00000003,
		RenamedOldName	= 0x00000004,
		RenamedNewName	= 0x00000005,
	};

	// Completion key (i.e. context pointer) associated with the I/O completion port.
	// Note that we don't currently use this for anything but set it to an arbitrary value for
	// verification using some asserts.
	const auto CompletionKey = ULONG_PTR{ 0x88448844 };

}


namespace fmt
{

	template <>
	struct formatter<FileNotifyInfoAction, wchar_t>
	{
		template <typename ParseContext>
		constexpr auto parse(ParseContext& ctx) { return ctx.begin(); }

		template <typename FormatContext>
		auto format(FileNotifyInfoAction action, FormatContext& ctx)
		{
			return format_to(ctx.out(), L"{}", [=] {
				switch(action)
				{
					case Added:				return L"ADDED";
					case Removed:			return L"REMOVED";
					case Modified:			return L"MODIFIED";
					case RenamedOldName:	return L"RENAMED_OLD_NAME";
					case RenamedNewName:	return L"RENAMED_NEW_NAME";
					default:				return L"<Unknown>";
				}
			}());
		}
	};

}


ChangeMonitor::ChangeMonitor()
{
	con.debug("Creating change monitor");

	// Create an unassociated I/O completion port
	hCompletionPort_ = ::CreateIoCompletionPort(INVALID_HANDLE_VALUE, NULL, 0, 1);
	if(hCompletionPort_ == NULL)
	{
		throw WinException(::GetLastError(), "Failed to create I/O completion port");
	}

	monitorThread_ = std::thread([this] {
		con.debug("Starting monitor thread");
		for(;;)
		{
			auto dwBytesTransferred = DWORD{};
			auto completionKey = ULONG_PTR{};
			auto pOverlapped = LPOVERLAPPED{};
			const auto success = ::GetQueuedCompletionStatus(
				hCompletionPort_,
				&dwBytesTransferred,
				&completionKey,
				&pOverlapped,
				INFINITE
			);
			if(success == FALSE)
			{
				if(pOverlapped == NULL)
				{
					con.error("Infinity timed out! Entering a new spacetime.");
					assert(false);
					continue;	// Timeout => continue
				}
				else
				{
					const auto error = ::GetLastError();
					con.error(L"Failed to get completion status: {}", error);
					assert(completionKey == CompletionKey);
					break;		// Error => give up
				}
			}
			else
			{
				assert(completionKey == CompletionKey);
				if(pOverlapped == NULL)
				{
					// This is a completion status posted by the ~ChangeMonitor destructor.
					break;			// Exit the thread
				}
			}

			// The address of the OVERLAPPED structure is also the address of our Dir object
			auto& dir = reinterpret_cast<Dir&>(*pOverlapped);

			// Immediately watch for more changes.
			// This doesn't guarantee that we don't miss events but may be as good as it gets. (?)
			ReadChangesOverlapped(dir);

			// Parse the event buffer
			ProcessChanges(dir, reinterpret_cast<FILE_NOTIFY_INFORMATION *>(dir.EventBuffer.data()));
		}
		con.debug("Exiting monitor thread");
	});
}


ChangeMonitor::~ChangeMonitor()
{
	con.debug("Destroying change monitor");

	if(monitorThread_.joinable())
	{
		assert(hCompletionPort_ != INVALID_HANDLE_VALUE);
		if(const auto res = ::PostQueuedCompletionStatus(hCompletionPort_, 0, CompletionKey, nullptr);
		   res == 0)
		{
			const auto error = ::GetLastError();
			con.critical(L"Failed to post completion status; unable to stop worker thread: {}", error);
			std::terminate();	// 881
		}

		con.debug("Joining monitor thread");
		monitorThread_.join();
		con.debug("Monitor thread joined");
	}

	::CloseHandle(hCompletionPort_);
	hCompletionPort_ = INVALID_HANDLE_VALUE;
}


void
ChangeMonitor::SetEventHandler(EventHandler handler)
{
	eventHandler_ = std::move(handler);
}


bool
ChangeMonitor::AddDirectory(const std::wstring& dirName)
{
	con.info(L"Monitoring directory '{}'", dirName);

	auto hDir = ::CreateFile(
		dirName.c_str(),
		FILE_LIST_DIRECTORY,
		FILE_SHARE_WRITE | FILE_SHARE_READ | FILE_SHARE_DELETE,
		nullptr,
		OPEN_EXISTING,
		FILE_FLAG_OVERLAPPED | FILE_FLAG_BACKUP_SEMANTICS,
		nullptr
	);
	if(hDir == INVALID_HANDLE_VALUE)
	{
		const auto error = ::GetLastError();
		con.error(L"Failed to create handle for monitoring directory '{}': {}", dirName, error);
		return false;
	}

	// Close the handle if something goes wrong
	auto closeDir = make_scope_guard([&] { ::SafeCloseHandle(hDir); });

	if(const auto hCp = ::CreateIoCompletionPort(hDir, hCompletionPort_, CompletionKey, 0); hCp == NULL)
	{
		const auto error = ::GetLastError();
		con.error(L"Failed to associate directory handle for '{}' with I/O completion port: {}", dirName, error);
		return false;
	}
	else
	{
		assert(hCp == hCompletionPort_);
	}

	// Append the directory information to our list of monitored directories.
	// We do this before calling ReadChangesOverlapped() because Dir is neither copyable nor movable,
	// so we can't create it and then copy/move it into the linked list.
	auto& dir = directories_.emplace_back(dirName, hDir);

	// Start monitoring
	if(const auto success = ReadChangesOverlapped(dir);
	   !success)
	{
		directories_.pop_back();	// Remove what we just added
		return false;
	}

	closeDir.dismiss();			// We've made it
	return true;
}


bool
ChangeMonitor::ReadChangesOverlapped(Dir& dir)
{
	const auto success = ::ReadDirectoryChangesW(
		dir.Handle, dir.EventBuffer.data(), static_cast<DWORD>(dir.EventBuffer.size()), TRUE,
		FILE_NOTIFY_CHANGE_FILE_NAME | FILE_NOTIFY_CHANGE_DIR_NAME | FILE_NOTIFY_CHANGE_ATTRIBUTES | FILE_NOTIFY_CHANGE_SIZE,
		nullptr,
		&dir.Overlapped, nullptr
	);
	if(!success)
	{
		const auto error = ::GetLastError();
		con.error("Failed to register for directory changes: {}", error);
		return false;
	}

	return true;
}


void
ChangeMonitor::ProcessChanges(const Dir& dir, const FILE_NOTIFY_INFORMATION *list) const
{
	auto entry = list;
	while(entry != nullptr)
	{
		const auto fileName = std::wstring{ entry->FileName, entry->FileNameLength / sizeof(entry->FileName[0]) };
		const auto filePath = dir.Name + L"\\" + fileName;
		const auto action = static_cast<FileNotifyInfoAction>(entry->Action);

		con.info(L"{{ FilePath: '{}', Action: {}, NextEntryOffset: {} }}", filePath, action, entry->NextEntryOffset);

		// We only care about 'Added', 'Modified', and 'RenamedNewName'.
		// The 'Modified' event sounds more promising than it really is because adding a new stream
		// to an existing file, e.g. in PowerShell, doesn't trigger a notification. Some programs,
		// Slack for example, do however modify their files at some point after first creating them
		// without 'Zone.Identifier' stream and later adding that stream. This is an opportunity to
		// catch such cases without having to introduce a general delay.
		if(action == Added || action == RenamedNewName || action == Modified)
		{
			if(eventHandler_)
			{
				const auto event = Event{ filePath };
				eventHandler_(event);
			}
		}

		if(entry->NextEntryOffset == 0)
			entry = nullptr;
		else
			entry = reinterpret_cast<const FILE_NOTIFY_INFORMATION *>(reinterpret_cast<const uint8_t *>(entry) + entry->NextEntryOffset);
	}
}
