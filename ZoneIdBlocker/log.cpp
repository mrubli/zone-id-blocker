#include "stdafx.h"

#define NOMINMAX
#include <Windows.h>

#include "log.hpp"


namespace
{

	auto ConsoleLoggerPtr = std::make_shared<spdlog::logger>("zib");

}


spdlog::logger& con = *ConsoleLoggerPtr;


bool
SetupLogging() try
{
	// See: https://github.com/gabime/spdlog/issues/762
	::SetConsoleOutputCP(CP_UTF8);

	// Create a OutputDebugStringA sink for the console
	auto console_sink = std::make_shared<spdlog::sinks::msvc_sink_mt>();

	// Use the console for the console logger
	con.sinks().push_back(console_sink);

	// Set the log level at the logger level, not the sink level
	con.set_level(spdlog::level::debug);

	// Make our console logger the global logger, so that libraries we load use it by default
	spdlog::set_default_logger(ConsoleLoggerPtr);

	return true;
}
catch(const spdlog::spdlog_ex& ex)
{
	OutputDebugStringA(fmt::format("ERROR: Logging setup failed: {}", ex.what()).c_str());
	return false;
}
