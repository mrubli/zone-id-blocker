#include "stdafx.h"

#include <chrono>
#include <optional>
#include <vector>

#include "config.hpp"

#include "StreamZapper.hpp"


using namespace std::chrono_literals;


namespace
{

	constexpr auto NumRetries = 3;

	constexpr auto RetryDelay = 500ms;


	std::optional<size_t>
	BackupRead(
		HANDLE	hFile,
		void *	buffer,
		size_t  numBytesToRead,
		LPVOID *pContext
	)
	{
		auto dwNumberOfBytesRead = DWORD{};
		const auto success = ::BackupRead(
			hFile,
			reinterpret_cast<BYTE *>(buffer),
			static_cast<DWORD>(numBytesToRead),
			&dwNumberOfBytesRead,
			FALSE,
			FALSE,
			pContext
		);
		if(!success)
		{
			const auto error = ::GetLastError();
			con.error("BackupRead() failed: {}", error);
			return {};	// Error
		}
		if(dwNumberOfBytesRead == 0)
			return 0;	// All the data associated with the file handle has been read
		if(dwNumberOfBytesRead < numBytesToRead)
		{
			con.error("BackupRead() succeeded but read only {} instead of {} bytes", dwNumberOfBytesRead, numBytesToRead);
			return {};	// Error
		}
		return dwNumberOfBytesRead;
	}

}


StreamZapper::StreamZapper()
{
	thread_ = std::thread{ [this] { Thread(); } };
}


StreamZapper::~StreamZapper()
{
	if(thread_.joinable())
	{
		exitThread_ = true;
		workToDo_.notify_all();
		thread_.join();
	}
}


void
StreamZapper::SetEventHandler(EventHandler handler)
{
	eventHandler_ = std::move(handler);
}


bool
StreamZapper::QueueFile(const std::wstring& filePath)
{
	{
		auto lock = std::scoped_lock{ mutex_ };
		queue_.push_back(File{ filePath, NumRetries });
	}
	workToDo_.notify_all();

	return true;
}


void
StreamZapper::Thread()
{
	while(!exitThread_)
	{
		auto lock = std::unique_lock{ mutex_ };
		if(queue_.empty())
		{
			// The lambda below returns true if we're ready to continue (i.e. should stop waiting)
			workToDo_.wait(lock, [this] { return exitThread_ || !queue_.empty(); });
			// The mutex has been reacquired by now.
		}

		auto retryItems = std::vector<File>{};
		while(!exitThread_ && !queue_.empty())
		{
			auto file = std::move(queue_.front());
			queue_.pop_front();

			// Process the file while the mutex is released
			lock.unlock();
			if(const auto success = ProcessFile(file);
				!success)
			{
				if(file.RetriesLeft-- > 0)
				{
					con.info(L"File '{}' has {} retries left. Trying again later.", file.Path, file.RetriesLeft + 1);
					retryItems.push_back(std::move(file));
				}
				else
				{
					con.error(L"Giving up on file '{}' after {} attempts.", file.Path, NumRetries + 1);
				}
			}
			lock.lock();
		}

		// Move to-be-retried items back into the queue and wait for a short time
		if(!retryItems.empty())
		{
			queue_.insert(std::end(queue_), std::begin(retryItems), std::end(retryItems));
			std::this_thread::sleep_for(RetryDelay);
		}
	}
}


bool
StreamZapper::ProcessFile(const File& file)
{
	if(const auto badStreams = ScanForBadStreams(file.Path);
	   badStreams)
	{
		if(!badStreams->empty())
		{
			if(const auto success = DeleteStreams(file.Path, *badStreams);
			   !success)
			{
				return false;	// Retry
			}
		}
	}
	else
	{
		if(badStreams.error() == ERROR_SHARING_VIOLATION)
		{
			return false;	// Retry
		}
	}
	return true;
}


// Notes:
// - The basic structure of the byte stream delivered by BackupRead is:
//     +--------------------------+---------------+--------------------------+---------------+-----
//     | WIN32_STREAM_ID + Name 0 | Stream data 0 | WIN32_STREAM_ID + Name 1 | Stream data 1 | ...
//     +--------------------------+---------------+--------------------------+---------------+-----
//
// - WIN32_STREAM_ID is actually only 20 bytes plus a WCHAR cStreamName[1] array at the end, so
//   22 bytes plus 2 padding bytes. BackupRead will write the name into the padding, i.e. it
//   ignores sizeof(WIN32_STREAM_ID::cStreamName) and writes up to nNumberOfBytesToRead bytes.
//
// - According to the BackupRead documentation the buffer must be "greater than the size of a
//   WIN32_STREAM_ID structure", so > 24 bytes. However, the function does not fail if a buffer of
//   size sizeof(WIN32_STREAM_ID) is passed and, in fact, even smaller and zero buffers are fine.
//
// - Stream names in WIN32_STREAM_ID::cStreamName appear exactly like they do in 'streams', e.g.
//   ":Zone.Identifier:$DATA" or ":x:$DATA", without null-terminator.
//
// - For files with an empty main stream and a non-empty extra stream the first bytes returned by
//   BackupRead contain the WIN32_STREAM_ID for the extra stream.
basic_result<std::vector<std::wstring>, DWORD>
StreamZapper::ScanForBadStreams(const std::wstring& filePath)
{
	con.info(L"Scanning file '{}' for unwanted streams:", filePath);

	const auto hFile = ::CreateFile(
		filePath.c_str(),
		GENERIC_READ,
		FILE_SHARE_WRITE | FILE_SHARE_READ | FILE_SHARE_DELETE,
		nullptr,
		OPEN_EXISTING,
		FILE_ATTRIBUTE_NORMAL,
		nullptr
	);
	if(hFile == INVALID_HANDLE_VALUE)
	{
		const auto error = ::GetLastError();
		con.error(L"Unable to open file '{}' for enumerating streams: {}", filePath, error);
		return error;
	}

	auto badStreams = std::vector<std::wstring>{};

	auto si = WIN32_STREAM_ID{};
	VOID *pContext = nullptr;

	for(;;)
	{
		auto totalBytesRead = size_t{ 0 };

		// Read the stream information (plus the first few bytes of the name)
		{
			const auto bytesRead = BackupRead(hFile, &si, sizeof(si), &pContext);
			if(bytesRead.value_or(0) == 0)
				break;	// Exit on error or once all data has been read
			totalBytesRead += *bytesRead;
		}

		// If the stream has a name (i.e. if it's an alternate stream) read the first of the name
		auto streamName = std::wstring{};
		if(si.dwStreamNameSize > 0)
		{
			static_assert(std::is_same_v<WCHAR,
			                             std::remove_reference_t<decltype(WIN32_STREAM_ID::cStreamName[0])>
			                            >);

			// Copy the part of the name that's contained in the WIN32_STREAM_ID buffer
			const auto maxNameCharsInSi = (sizeof(WIN32_STREAM_ID) - offsetof(WIN32_STREAM_ID, cStreamName)) / sizeof(WCHAR);
			const auto nameCharsInSi = std::min(si.dwStreamNameSize / sizeof(WCHAR), maxNameCharsInSi);
			streamName = std::wstring{ si.cStreamName, nameCharsInSi };

			// Read the rest of the name
			const auto nameCharsLeft = (si.dwStreamNameSize / sizeof(WCHAR)) - nameCharsInSi;
			auto buffer = std::vector<WCHAR>(nameCharsLeft);
			const auto bytesRead = BackupRead(hFile, buffer.data(), buffer.size() * sizeof(WCHAR), &pContext);
			if(bytesRead.value_or(0) == 0)
				break;	// Exit on error
			totalBytesRead += *bytesRead;

			streamName.append(std::begin(buffer), std::end(buffer));
		}

		// Check if the stream name is on our black list
		con.info(L"  {{ Name: '{}', Id: {}, Size: {} }}", streamName, si.dwStreamId, si.Size.QuadPart);
		if(IsBadStream(streamName))
		{
			badStreams.push_back(streamName);
		}

		// Seek to the next stream
		{
			auto bytesToSeek = LARGE_INTEGER{};
			bytesToSeek.QuadPart = si.Size.QuadPart - (totalBytesRead - offsetof(WIN32_STREAM_ID, cStreamName) - si.dwStreamNameSize);
			if(bytesToSeek.QuadPart > 0)
			{
				auto dwLowByteSeeked = DWORD{}, dwHighByteSeeked = DWORD{};
				const auto success = ::BackupSeek(
					hFile,
					bytesToSeek.LowPart, bytesToSeek.HighPart,
					&dwLowByteSeeked, &dwHighByteSeeked,
					&pContext
				);
				if(!success)
				{
					const auto error = ::GetLastError();
					con.error("BackupSeek() failed: {}", error);
					break;
				}
			}
		}
	}

	// Abort the backup
	{
		const auto success = ::BackupRead(
			hFile,
			nullptr,
			0,
			nullptr,
			TRUE,
			FALSE,
			&pContext
		);
		if(!success)
		{
			const auto error = ::GetLastError();
			con.error("BackupRead(bAbort = TRUE) failed: {}", error);
			// Ignore the error
		}
	}

	::CloseHandle(hFile);

	return badStreams;
}


bool
StreamZapper::DeleteStreams(const std::wstring& filePath, const std::vector<std::wstring>& streamNames)
{
	auto overallSuccess = true;

	for(const auto& streamName : streamNames)
	{
		const auto streamPath = filePath + streamName;
		con.info(L"Removing evil stream: '{}'", streamPath);

		const auto success = ::DeleteFile(streamPath.c_str());
		if(!success)
		{
			const auto error = ::GetLastError();
			con.error(L"Unable to delete stream '{}': {}", streamPath, error);
		}

		// Notify listeners of our good deed (or failure thereof)
		if(eventHandler_)
		{
			eventHandler_(Event{ filePath, streamName, success != 0 });
		}

		overallSuccess &= (success != 0);
	}

	return overallSuccess;
}


bool
StreamZapper::IsBadStream(const std::wstring& streamName)
{
	if(streamName.empty() || streamName == L":$DATA")
		return false;	// Refuse to delete the default stream
	return contains(Settings.UnwantedStreams, streamName);
}
