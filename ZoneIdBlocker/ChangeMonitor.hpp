#pragma once

#include <array>
#include <functional>
#include <string>
#include <thread>
#include <list>

#define NOMINMAX
#include <Windows.h>


constexpr auto EventBufferSize = 16 * 1024;	// TODO tune size


class ChangeMonitor
{
public:
	ChangeMonitor();
	~ChangeMonitor();

	// Explicitly disallow copying
	ChangeMonitor(const ChangeMonitor&) = delete;
	ChangeMonitor& operator=(const ChangeMonitor&) = delete;

	struct Event
	{
		const std::wstring&		FileName;
	};
	using EventHandler = std::function<void(const Event&)>;
	void			SetEventHandler(EventHandler handler);

	bool			AddDirectory(const std::wstring& dirName);

private:
	EventHandler		eventHandler_;

	HANDLE				hCompletionPort_{ INVALID_HANDLE_VALUE };
	std::thread			monitorThread_;

	struct Dir
	{
		// The OVERLAPPED struct is the first member, so that &Dir == &Dir::Overlapped.
		OVERLAPPED								Overlapped{};
		std::wstring							Name;
		HANDLE									Handle{};
		// Placing the big array right into the struct makes sense because the struct is contained
		// in a linked list, so a single allocation can allocate the item and the buffer.
		std::array<uint8_t, EventBufferSize>	EventBuffer{};

		Dir(const std::wstring& name, HANDLE handle)
			: Name{ name }
			, Handle{ handle }
		{}

		// This struct must be non-movable because we pass the address of the OVERLAPPED struct to
		// Windows API functions.
		// We could make it copyable to allow insertion using std::list::push_back() but having
		// it non-copyable avoids bugs. std::list::emplace_back() still works.
		Dir(const Dir&) = delete;
		Dir(Dir&&) = delete;
		Dir& operator=(const Dir&) = delete;
		Dir& operator=(Dir&&) = delete;
	};
	// Use a linked list to guarantee that the Dir objects never change address.
	std::list<Dir>	directories_;	// TODO consider thread-safety

private:
	bool			ReadChangesOverlapped(Dir& dir);
	void			ProcessChanges(const Dir& dir, const FILE_NOTIFY_INFORMATION *list) const;
};