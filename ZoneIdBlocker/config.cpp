﻿#include "stdafx.h"

#include <fstream>
#include <sstream>

#pragma warning(push)
#pragma warning(disable:4996)
#include <toml/toml.h>
#pragma warning(pop)

#include "config.hpp"


ProgramSettings Settings = {};


namespace fmt
{

	template <>
	struct formatter<ProgramSettings, wchar_t>
	{
		template <typename ParseContext>
		constexpr auto parse(ParseContext& ctx) { return ctx.begin(); }

		template <typename FormatContext>
		auto format(const ProgramSettings& ps, FormatContext& ctx)
		{
			auto it = ctx.out();
			it = format_to(ctx.out(), L"{{\r\n  Directories: [");
			for(auto i = size_t{}; i < ps.Directories.size(); i++)
			{
				it = format_to(it, L"{}'{}'", i > 0 ? L", " : L"", ps.Directories[i]);
			}
			it = format_to(ctx.out(), L"],\r\n  UnwantedStreams: [");
			for(auto i = size_t{}; i < ps.UnwantedStreams.size(); i++)
			{
				it = format_to(it, L"{}'{}'", i > 0 ? L", " : L"", ps.UnwantedStreams[i]);
			}
			it = format_to(it, L"]\r\n}}");
			return it;
		}
	};

}


namespace
{

	const auto SettingsFile = std::wstring{ L"ZoneIdBlocker.toml" };


	template<typename T>
	std::tuple<bool, std::wstring>
	read_setting(const toml::Value& table, const std::string& name, T& value)
	{
		con.debug("Reading setting '{}'.", name);

		const auto* v = table.find(name);
		if(v && v->is<T>())
		{
			value = v->as<T>();
			return { true, {} };
		}
		else
		{
			const auto error = fmt::format(L"Setting '{}' not found or its value has the wrong type.", ascii_to_wstring(name));
			con.error(L"{}", error);
			return { false, error };
		}
	}


	// Try to read whatever settings we can and collects errors as we go.
	void
	read_config_table(const toml::Table& table, ProgramSettings& settings,
					  std::vector<std::wstring>& errors, const std::string& tableName)
	{
		con.debug("Reading configuration section '{}'.", tableName);

		// Go over all the table's values first while skipping subtables
		for(auto&&[key, value] : table)
		{
			auto success = false;
			auto error = std::wstring{};
			if(key == "Directories")
			{
				auto values = std::vector<std::string>{};
				tie(success, error) = read_setting(table, key, values);
				if(!success)
				{
					errors.push_back(error);
				}
				else
				{
					settings.Directories.clear();
					std::transform(std::cbegin(values), std::cend(values),
								   std::back_inserter(settings.Directories),
								   [] (const auto& s) { return utf8_to_wstring(s); });
				}
			}
			else if(key == "UnwantedStreams")
			{
				auto values = std::vector<std::string>{};
				tie(success, error) = read_setting(table, key, values);
				if(!success)
				{
					errors.push_back(error);
				}
				else
				{
					settings.UnwantedStreams.clear();
					std::transform(std::cbegin(values), std::cend(values),
								   std::back_inserter(settings.UnwantedStreams),
								   [] (const auto& s) { return utf8_to_wstring(s); });
				}
			}
			else
			{
				error = fmt::format(L"Unsupported setting '{}' encountered.", ascii_to_wstring(key));
			}

			if(!success)
			{
				errors.push_back(error);
			}
		}
	}


	template<typename T>
	bool
	write_setting(std::ostream& os, const char *name, const T& value)
	{
		os << name << " = " << toml::Value{ value } << std::endl;
		return true;
	}

	bool
	write_setting(std::ostream& os, const char *name, const std::vector<std::wstring>& values)
	{
		os << name << " = [\n";
		for(auto&& v : values)
			os << "\t\"" << wstring_to_utf8(v) << "\",\n";
		os << "]" << std::endl;
		return true;
	}

}


void
init_default_config()
{
	con.info("Using default settings.");
	Settings = {};
}


std::tuple<ConfigResult, std::wstring>
read_config_file()
{
	con.info(L"Reading settings file '{}' ...", SettingsFile);

	// Open the file
	std::ifstream is{ SettingsFile, std::ios_base::in };
	if(!is)
	{
		const auto error = fmt::format(L"Failed to open settings file '{}'.", SettingsFile);
		con.error(error);
		return { ConfigResult::FileNotFound, error };
	}

	// Parse it using the TOML parser.
	// Note that the root value is always of type TABLE_TYPE, even if the file is empty or contains no tables.
	auto pr = toml::parse(is);
	if(!pr.valid() || pr.value.type() != toml::Value::TABLE_TYPE)
	{
		const auto error = fmt::format(L"Failed to parse settings file '{}': {}", SettingsFile, ascii_to_wstring(pr.errorReason));
		con.error(error);
		return { ConfigResult::SyntaxError, error };
	}
	
	const auto *settingsTable = pr.value.findChild("settings");
	if(!settingsTable)
	{
		const auto error = L"Settings file is missing the [settings] section.";
		con.error(error);
		return { ConfigResult::SyntaxError, error };
	}

	{
		// Start off with the default settings
		auto newSettings = ProgramSettings{};

		auto errors = std::vector<std::wstring>{};
		if(settingsTable)
		{
			read_config_table(settingsTable->as<toml::Table>(), newSettings, errors, "settings");
		}

		// Use the settings we just read (no matter complete or not)
		Settings = newSettings;

		// Return an error if there were any errors while reading settings
		if(!errors.empty())
		{
			std::wstringstream ss;
			for(auto i = 0; i < errors.size(); i++)
			{
				if(i > 0)
					ss << L"\r\n";
				ss << L"  ▪  " << errors[i];
			}
			return make_pair(ConfigResult::ConfigError, ss.str());
		}
	}

	con.info(L"Successfully read settings file '{}': {}", SettingsFile, Settings);
	return { ConfigResult::Success, {} };
}


bool
write_config_file()
{
	con.info(L"Writing settings file '{}' ...", SettingsFile);

	auto os = std::ofstream{ SettingsFile, std::ios_base::out | std::ios_base::trunc };
	if(!os)
	{
		con.error(L"Failed to write settings file '{}'.", SettingsFile);
		return false;
	}

	os << "[settings]\n";

	write_setting(os, "Directories",	Settings.Directories);

	con.info(L"Successfully wrote settings file '{}'.", SettingsFile);
	return true;
}
