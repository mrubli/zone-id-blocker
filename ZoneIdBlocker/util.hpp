#pragma once

#include <algorithm>
#include <string>
#include <string_view>

#define NOMINMAX
#include <Windows.h>


inline
std::wstring
ascii_to_wstring(std::string_view str)
{
	return { std::begin(str), std::end(str) };
}


std::string
wstring_to_utf8(const std::wstring& str);


std::wstring
utf8_to_wstring(const std::string& str);


template<typename Container, typename T>
bool
contains(const Container& container, T&& value)
{
	auto&& end = std::cend(container);
	return std::find(std::cbegin(container), end, value) != end;
}


inline
BOOL
SafeCloseHandle(HANDLE& hObject)
{
	const auto ret = ::CloseHandle(hObject);
	if(ret != 0)
	{
		hObject = INVALID_HANDLE_VALUE;
	}
	return ret;
}
