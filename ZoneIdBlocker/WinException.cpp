#include "stdafx.h"

#include "WinException.hpp"

#include <sstream>


using namespace std;


WinException::WinException(DWORD errorCode, const std::string& preamble)
	 : errorCode_(errorCode)
{
	stringstream ss;

	if(!preamble.empty())
		ss << preamble << ": ";

	char *pBuffer = NULL;
	DWORD res = ::FormatMessageA(
		FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL,
		errorCode_,
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		(LPSTR)&pBuffer,
		0,
		NULL
	);
	if(res == 0)
	{
		ss << "Unknown error [Failed to look up error message]";
	}
	else
	{
		// Remove trailing \n and \r characters
		while(strlen(pBuffer) > 0 && (pBuffer[strlen(pBuffer) - 1] == '\n' || pBuffer[strlen(pBuffer) - 1] == '\r'))
			pBuffer[strlen(pBuffer) - 1] = '\0';

		ss << pBuffer;
		::LocalFree(pBuffer);
	}

	ss << " (Error: 0x" << hex << noshowbase << uppercase << errorCode_ << dec << ")";

	error_ = ss.str();
	werror_ = wstring(error_.cbegin(), error_.cend());
}


WinException::WinException(DWORD errorCode, const std::wstring& preamble)
	 : errorCode_(errorCode)
{
	wstringstream ss;

	if(!preamble.empty())
		ss << preamble << ": ";

	wchar_t *pBuffer = NULL;
	DWORD res = ::FormatMessageW(
		FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL,
		errorCode_,
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		(LPWSTR)&pBuffer,
		0,
		NULL
	);
	if(res == 0)
	{
		ss << L"Unknown error [Failed to look up error message]";
	}
	else
	{
		// Remove trailing \n and \r characters
		while(wcslen(pBuffer) > 0 && (pBuffer[wcslen(pBuffer) - 1] == L'\n' || pBuffer[wcslen(pBuffer) - 1] == L'\r'))
			pBuffer[wcslen(pBuffer) - 1] = L'\0';

		ss << pBuffer;
		::LocalFree(pBuffer);
	}

	ss << L" (Error: 0x" << hex << noshowbase << uppercase << errorCode_ << dec << L")";

	werror_ = ss.str();
	error_ = string(werror_.cbegin(), werror_.cend());
}


const char *
WinException::what() const
{
	return error_.c_str();
}


const wchar_t *
WinException::wwhat() const
{
	return werror_.c_str();
}
