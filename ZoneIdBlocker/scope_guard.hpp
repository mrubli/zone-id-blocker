/*
 * Scope guard helper class from Andrei Alexandrescu's 2012 "Systematic Error Handling in C++" talk
 */

#pragma once

#include <type_traits>
#include <utility>


template<typename F>
class scope_guard
{
	F f_;
	bool active_;

public:
	scope_guard() = delete;
	scope_guard(F f) : f_{ std::move(f) }, active_{ true } {}
	scope_guard(const scope_guard&) = delete;
	scope_guard& operator=(const scope_guard&) = delete;
	scope_guard(scope_guard&& rhs) : f_{ std::move(rhs.f_) }, active_{ rhs.active_ } { rhs.dismiss(); }

	~scope_guard() { if(active_) f_(); }

	void dismiss() { active_ = false; }
};

template<typename F>
scope_guard<std::decay_t<F>>
make_scope_guard(F&& f)
{
	return scope_guard<std::decay_t<F>>(std::forward<F>(f));
}


namespace detail
{
	enum class scope_guard_on_exit {};

	template <typename F>
	scope_guard<F>
	operator+(scope_guard_on_exit, F&& fn)
	{
		return scope_guard<F>{ std::forward<F>(fn) };
	}
}

#define CONCATENATE_IMPL(s1, s2) s1##s2
#define CONCATENATE(s1, s2) CONCATENATE_IMPL(s1, s2)
#ifdef __COUNTER__
	#define ANONYMOUS_VARIABLE(str) CONCATENATE(str, __COUNTER__)
#else
	#define ANONYMOUS_VARIABLE(str) CONCATENATE(str, __LINE__)
#endif

#define SCOPE_EXIT \
	auto ANONYMOUS_VARIABLE(SCOPE_EXIT_STATE) = ::detail::scope_guard_on_exit{} + [&] ()
