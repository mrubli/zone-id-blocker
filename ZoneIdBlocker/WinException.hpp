#pragma once

#include "stdafx.h"

#include <string>
#include <stdexcept>

#define WIN32_LEAN_AND_MEAN
#define NOMINMAX
#include <Windows.h>


class WinException
	: public std::exception
{
public:
	WinException(DWORD errorCode, const std::string& preamble = std::string());
	WinException(DWORD errorCode, const std::wstring& preamble = std::wstring());

	const char *		what() const override;
	const wchar_t *		wwhat() const;

private:
	DWORD				errorCode_;
	std::string			error_;
	std::wstring		werror_;
};
